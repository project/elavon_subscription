<?php

/**
 * @file
 * Rules integratrion for the Elavon Subscription module.
 */

/**
 * Implements hook_rules_data_info().
 */
function elavon_subscription_rules_data_info() {
  return array(
    'elavon_credentials' => array(
      'label' => t('Elavon credentials array data'),
      'group' => t('Elavon Subscription'),
    ),
    'elavon_subscription' => array(
      'label' => t('Elavon subscription array data'),
      'group' => t('Elavon Subscription'),
    ),
    'elavon_subscriptions' => array(
      'label' => t('Elavon subscriptions array'),
      'group' => t('Elavon Subscription'),
    ),
    'elavon_new_subscription_response' => array(
      'label' => t('Elavon new subscription response array data'),
      'group' => t('Elavon Subscription'),
    ),
  );
}

/**
 * Implements hook_rules_event_info().
 */
function elavon_subscription_rules_event_info() {
  return array(
    'elavon_subscription_process_subscriptions' => array(
      'label' => t('Process Elavon subscriptions'),
      'group' => t('Elavon Subscription'),
      'module' => 'elavon_subscription',
    ),
  );
}

/**
 * Implements hook_rules_action_info().
 */
function elavon_subscription_rules_action_info() {
  return array(
    'elavon_subscription_process_new_subscription' => array(
      'label' => t('Process a new Elavon subscription'),
      'group' => t('Elavon Subscription'),
      'parameter' => array(
        'subscription' => array(
          'type' => 'elavon_subscription',
          'label' => t('Subscription'),
        ),
        'credentials' => array(
          'label' => t('Elavon credentials array'),
          'type' => 'elavon_credentials',
        ),
      ),
      'provides' => array(
        'response_result' => array(
          'label' => t('The result code of the request.'),
          'type' => 'integer',
        ),
        'response_message' => array(
          'label' => t('A message describing the result code.'),
          'type' => 'text',
        ),
        'response_merchant_id' => array(
          'label' => t('The client id as in the payandshop..client_details table.'),
          'type' => 'text',
        ),
      ),
    ),
    'elavon_subscription_provide_elavon_credentials' => array(
      'label' => t('Provide an Elavon credentials for a subscription'),
      'group' => t('Elavon Subscription'),
      'parameter' => array(
        'merchant_id' => array(
          'label' => t('Client ID'),
          'description' => t('The client id as in the payandshop..client_details table.'),
          'type' => 'text',
        ),
        'account' => array(
          'label' => t('Account name'),
          'description' => t('The name of the account against which to process the transaction.'),
          'type' => 'text',
        ),
        'shared_secret' => array(
          'label' => t('Shared Secret'),
          'description' => t('The shared secret for the Elavon account.'),
          'type' => 'text',
        ),
        'server_url' => array(
          'label' => t('Server URL'),
          'description' => t('The URL to the Elavon server where payments are processed.'),
          'type' => 'text',
        ),
      ),
      'provides' => array(
        'elavon_credentials' => array(
          'label' => t('Elavon credentials array'),
          'type' => 'elavon_credentials',
        ),
      ),
    ),
    'elavon_subscription_provide_new_subscription' => array(
      'label' => t('Provide a new Elavon subscription'),
      'group' => t('Elavon Subscription'),
      'parameter' => array(
        'subscription_id' => array(
          'type' => 'text',
          'label' => t('Subscription ID'),
          'description' => t('A unique reference for this schedule - used when referring to it in subsequent requests.'),
        ),
        'alias' => array(
          'type' => 'text',
          'label' => t('Alias'),
          'description' => t('An alias or name given to this schedule - this can be used to identify multiple schedules for ease of identification. For example, "gym membership" or "monthly subscription".'),
          'optional' => TRUE,
        ),
        'payer_ref' => array(
          'type' => 'text',
          'label' => t('Payer ref'),
          'description' => t('The payer ref to take the payment from.'),
        ),
        'pmt_ref' => array(
          'type' => 'text',
          'label' => t('PMT ref'),
          'description' => t('The payment method belonging to that payer ref.'),
        ),
        'preffix' => array(
          'type' => 'text',
          'label' => t('Schedule preffix'),
          'description' => t('This will be used as the prefix of the order id. The order id will be constructed as follows: orderidstub-scheduleref-runid-attemptid.'),
          'optional' => TRUE,
        ),
        'currency' => array(
          'type' => 'text',
          'label' => t('Currency'),
          'description' => t('The currency to use.'),
        ),
        'amount' => array(
          'type' => 'text',
          'label' => t('Amount'),
          'description' => t('The fixed amount to process each time.'),
        ),
        'schedule' => array(
          'type' => 'text',
          'label' => t('Schedule'),
          'description' => t('The schedule to follow. Must consist of 3 space/tab separated items - one each for day-of-month, month and day-of-week or a macro name.'),
          'options list' => 'elavon_subscription_options_list_schedule',
        ),
        'startdate' => array(
          'type' => 'integer',
          'label' => t('Start date'),
          'description' => t('The startdate for this schedule in the format: yyyymmdd. Does not need to be an actual date that the schedule will run - it can be any date in the future. The schedule will not run before this date.'),
          'optional' => TRUE,
        ),
        'numtimes' => array(
          'type' => 'integer',
          'label' => t('Number times'),
          'description' => t('The number of times to process this schedule, or -1 for indefinitely. Must also be set to -1 if enddate is used. Max of 999.'),
        ),
        'enddate' => array(
          'type' => 'integer',
          'label' => t('End date'),
          'description' => t('The end date for this schedule in the format: yyyyddmm. Does not need to be an actual date that the schedule will run - it can be any date in the future and after startdate (if set). The schedule will not run any more times after this date. If the enddate is set then the numtimes must be set to -1.'),
          'optional' => TRUE,
        ),
      ),
      'provides' => array(
        'subscription' => array(
          'type' => 'elavon_subscription',
          'label' => t('A new Elavon subscription data'),
        ),
      ),
    ),
  );
}

/**
 * Rules action: Process and return the Elavon subsciption response.
 *
 * @param elavon_subscription $subscription
 *  An array containing an Elavon subscription, containing:
 *    subscription_id: A unique reference for this schedule - used when
 *      referring to it in subsequent requests.
 *    alias: An alias or name given to this schedule - this can be used to
 *      identify multiple schedules for ease of identification. For example,
 *      "gym membership" or "monthly subscription".
 *    payer_ref: The payer ref to take the payment from.
 *    pmt_ref: The payment method belonging to that payer ref.
 *    preffix: This will be used as the prefix of the order id. The order id
 *      will be constructed as follows: orderidstub-scheduleref-runid-attemptid.
 *    currency: The currency to use.
 *    amount: The fixed amount to process each time.
 *    schedule: The schedule to follow. Must consist of 3 space/tab separated
 *      items - one each for day-of-month, month and day-of-week or a macro
 *      name.
 *    startdate: The startdate for this schedule in the format: yyyymmdd. Does
 *      not need to be an actual date that the schedule will run - it can be any
 *      date in the future. The schedule will not run before this date.
 *    numtimes: The number of times to process this schedule, or -1 for
 *      indefinitely. Must also be set to -1 if enddate is used. Max of 999.
 *    endate: The end date for this schedule in the format: yyyyddmm. Does not
 *      need to be an actual date that the schedule will run - it can be any
 *      date in the future and after startdate (if set). The schedule will not
 *      run any more times after this date. If the enddate is set then the
 *      numtimes must be set to -1.
 * @param elavon_subscription_credentials $credentials
 *   An array with the Elavon credentials:
 *     merchant_id: The client id as in the payandshop..client_details table.
 *     account: The name of the account against which to process the
 *       transaction.
 *     shared_secret: The shared secret for the Elavon account.
 *     server_url: The URL to the Elavon server where payments are processed.
 *
 * @return array
 *   response_result: The result code of the request.
 *   response_message: A message describing the result code.
 *   response_merchant_id: The client id as in the payandshop..client_details
 *     table.
 */
function elavon_subscription_process_new_subscription($subscription, $credentials) {
  $response = array(
    'response_result' => '',
    'response_message' => '',
    'response_merchant_id' => '',
  );

  // Compose XML request data.
  $timestamp = format_date(REQUEST_TIME, 'custom', 'YmdHis');

  $xmlstr = '<request timestamp="' . $timestamp . '" type="schedule-new">';
  $xmlstr .= ' <merchantid>' . $credentials['merchant_id'] . '</merchantid>';
  $xmlstr .= ' <scheduleref>' . $subscription['subscription_id'] . '</scheduleref>';
  $xmlstr .= ' <alias>' . $subscription['alias'] . '</alias>';
  $xmlstr .= ' <payerref>' . $subscription['payer_ref'] . '</payerref>';
  $xmlstr .= ' <paymentmethod>' . $subscription['pmt_ref'] . '</paymentmethod>';
  $xmlstr .= ' <account>' . $credentials['account'] . '</account>';
  $xmlstr .= ' <orderidstub>' . $subscription['preffix'] . '</orderidstub>';
  $xmlstr .= ' <transtype>auth</transtype>';
  $xmlstr .= ' <amount currency="' . $subscription['currency'] . '">' . $subscription['amount'] . '</amount>';
  $xmlstr .= ' <prodid></prodid>';
  $xmlstr .= ' <varref></varref>';
  $xmlstr .= ' <custno></custno>';
  $xmlstr .= ' <comment></comment>';
  $xmlstr .= ' <schedule>' . $subscription['schedule'] . '</schedule>';
  $xmlstr .= ' <startdate>' . $subscription['startdate'] . '</startdate>';
  $xmlstr .= ' <numtimes>' . $subscription['numtimes'] . '</numtimes>';
  $xmlstr .= ' <enddate>' . $subscription['enddate'] . '</enddate>';
  $xmlstr .= '<sha1hash>' . sha1(sha1($timestamp . '.' . $credentials['merchant_id'] . '.' . $subscription['subscription_id'] . '.' . $subscription['amount'] . '.' . $subscription['currency'] . '.' . $subscription['payer_ref'] . '.' . $subscription['schedule']) . '.' . $credentials['shared_secret']) . '</sha1hash>';
  $xmlstr .= '</request>';

  // Post data and get the response.
  $response = drupal_http_request($credentials['server_url'], array(
    'headers' => array('Content-Type' => 'text/xml'),
    'data' => $xmlstr,
    'method' => 'POST',
    'timeout' => 15,
  ));

  if (empty($response->error)) {
    $data = new SimpleXMLElement($response->data);
    $data = drupal_json_decode(drupal_json_encode($data));

    if (!empty($data['result']) || (!empty($data['sha1hash']) && $data['sha1hash'] == sha1(sha1($data['@attributes']['timestamp'] . '.' . $data['merchantid'] . '.' . $data['result']) . '.' . $credentials['shared_secret']))) {
      // If the response is valid, store the response and return it.
      $response = array(
        'response_result' => $data['result'],
        'response_message' => $data['message'],
        'response_merchant_id' => $data['merchantid'],
      );
      if ($data['result'] != 0) {
        watchdog('elavon_subscription', 'Error processing an Elavon subscripton: (%code) %message', array('%code' => $data['result'], '%message' => $data['message']), WATCHDOG_ERROR);
      }
    }
    elseif ($data['result'] == 0 || (!empty($data['sha1hash']) && $data['sha1hash'] != sha1(sha1($data['@attributes']['timestamp'] . '.' . $data['merchantid'] . '.' . $data['result']) . '.' . $credentials['shared_secret']))) {
      watchdog('elavon_subscription', 'The SHA1 hash do not match.', WATCHDOG_ERROR);
    }
  }
  else {
    watchdog('elavon_subscription', 'Error processing an Elavon subscripton: %error', array('%error' => $response->error), WATCHDOG_ERROR);
  }

  return $response;
}

/**
 * Rules action: Get and retrun a setted Evalon credentials.
 *
 * @param string $merchant_id
 *   The client id as in the payandshop..client_details table.
 * @param string $account
 *   The name of the account against which to process the transaction.
 * @param string $shared_secret
 *   The shared secret for the Elavon account.
 * @param string $server_url
 *   The URL to the Elavon server where payments are processed.
 *
 * @return elavon_subscription_credentials
 *   An array with the Elavon credentials:
 *     merchant_id: The client id as in the payandshop..client_details table.
 *     account: The name of the account against which to process the
 *       transaction.
 *     shared_secret: The shared secret for the Elavon account.
 *     server_url: The URL to the Elavon server where payments are processed.
 */
function elavon_subscription_provide_elavon_credentials($merchant_id, $account, $shared_secret, $server_url) {
  return array(
    'elavon_credentials' => array(
      'merchant_id' => $merchant_id,
      'account' => $account,
      'shared_secret' => $shared_secret,
      'server_url' => $server_url,
    ),
  );
}

/**
 * Rules action: Provide a new Elavon subscription.
 *
 * @param string $subscription_id
 *   A unique reference for this schedule - used when referring to it in
 *   subsequent requests.
 * @param string $alias
 *   An alias or name given to this schedule - this can be used to identify
 *   multiple schedules for ease of identification. For example,
 * "gym membership" or "monthly subscription".
 * @param string $payer_ref
 *   The payer ref to take the payment from.
 * @param string $pmt_ref
 *   The payment method belonging to that payer ref.
 * @param string $preffix
 *   This will be used as the prefix of the order id. The order id will be
 *   constructed as follows: orderidstub-scheduleref-runid-attemptid.
 * @param string $currency
 *   The currency to use.
 * @param text $amount
 *   The fixed amount to process each time.
 * @param string $schedule
 *   The schedule to follow. Must consist of 3 space/tab separated items - one
 *   each for day-of-month, month and day-of-week or a macro name.
 * @param string $startdate
 *   The startdate for this schedule in the format: yyyymmdd. Does not need to
 *   be an actual date that the schedule will run - it can be any date in the
 *   future. The schedule will not run before this date.
 * @param string $numtimes
 *   he number of times to process this schedule, or -1 for indefinitely. Must
 *   also be set to -1 if enddate is used. Max of 999.
 * @param string $endate
 *   The end date for this schedule in the format: yyyyddmm. Does not need to be
 *   an actual date that the schedule will run - it can be any date in the
 *   future and after startdate (if set). The schedule will not run any more
 *   times after this date. If the enddate is set then the numtimes must be set
 *   to -1.
 *
 * @return elavon_subscription
 *  An array containing an Elavon subscription, containing:
 *    subscription_id: A unique reference for this schedule - used when
 *      referring to it in subsequent requests.
 *    alias: An alias or name given to this schedule - this can be used to
 *      identify multiple schedules for ease of identification. For example,
 *      "gym membership" or "monthly subscription".
 *    payer_ref: The payer ref to take the payment from.
 *    pmt_ref: The payment method belonging to that payer ref.
 *    preffix: This will be used as the prefix of the order id. The order id
 *      will be constructed as follows: orderidstub-scheduleref-runid-attemptid.
 *    currency: The currency to use.
 *    amount: The fixed amount to process each time.
 *    schedule: The schedule to follow. Must consist of 3 space/tab separated
 *      items - one each for day-of-month, month and day-of-week or a macro
 *      name.
 *    startdate: The startdate for this schedule in the format: yyyymmdd. Does
 *      not need to be an actual date that the schedule will run - it can be any
 *      date in the future. The schedule will not run before this date.
 *    numtimes: The number of times to process this schedule, or -1 for
 *      indefinitely. Must also be set to -1 if enddate is used. Max of 999.
 *    endate: The end date for this schedule in the format: yyyyddmm. Does not
 *      need to be an actual date that the schedule will run - it can be any
 *      date in the future and after startdate (if set). The schedule will not
 *      run any more times after this date. If the enddate is set then the
 *      numtimes must be set to -1.
 */
function elavon_subscription_provide_new_subscription($subscription_id, $alias, $payer_ref, $pmt_ref, $preffix, $currency, $amount, $schedule, $startdate, $numtimes, $endate) {
  return array(
    'subscription' => array(
      'subscription_id' => $subscription_id,
      'alias' => $alias,
      'payer_ref' => $payer_ref,
      'pmt_ref' => $pmt_ref,
      'preffix' => $preffix,
      'currency' => $currency,
      'amount' => $amount,
      'schedule' => $schedule,
      'startdate' => $startdate,
      'numtimes' => $numtimes,
      'enddate' => $endate,
    ),
  );
}

/**
 * Options list callback: Basics schedule to follow.
 */
function elavon_subscription_options_list_schedule() {
  return array(
    'weekly' => "Weekly on today's day",
    'monthly' => "Monthly on today's date",
    'bimonthly' => "Every Jan, Mar, May, Jul, Sept and Nov on today's date",
    'quarterly' => "Every Jan, Apr, Jul and Oct on today's date",
    'halfyearly' => "Every Jan and Jul on today's date",
    'yearly' => "Yearly on today's date",
  );
}
