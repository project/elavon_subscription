## Summary
@todo: add sumary.

## Dependencies
 * Rules

## Installation
 1. Place this module in the folder sites/all/modules/contrib.
 2. Go to the Module page at Administer > Modules
    (http://example.com/admin/modules) and enable it.

## Configuration and usage instructions
@todo: add configuration and usage instructions.

## Sponsors
 * This Drupal 7 module is sponsored and developed by http://cambrico.net/
   Get in touch with us for customizations and consultancy:
   http://cambrico.net/contact

## About the authors
 * Pedro Cambra (https://www.drupal.org/u/pcambra)
 * Manuel Egío (https://www.drupal.org/u/facine)
