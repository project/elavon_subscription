<?php

/**
 * @file
 * Default rule configurations for Commerce Elavon Example module.
 */

/**
* Implements hook_default_rules_configuration().
*/
function commerce_elavon_example_default_rules_configuration() {
  // Reset token static funcion to force reload token definitions.
  drupal_static_reset('token_info');
  token_info();

  $rules = array();

  // Add a reaction rule to process Elavon subscriptions.
  $rule = rules_reaction_rule();

  $rule->label = t('Process Elavon Subscriptions');
  $rule->tags = array('Commerce Payment');
  $rule->active = TRUE;
  $rule->dependencies = array(
    'rules',
    'elavon_subscription',
    'commerce_realex_redirect_subs',
    'commerce_elavon_subscription',
    'commerce_elavon_example',
  );

  $rule->event('elavon_subscription_process_subscriptions')
       ->action('entity_query', array(
         'type' => 'commerce_order',
         'property' => 'status',
         'value' => 'commerce_realex_pending',
         'limit' => 10,
         'entity_fetched:label' => t('Pending Elavon subscriptions'),
         'entity_fetched:var' => 'commerce_orders',
       ));

  // Build a loop that process every pending subscription.
  $loop = rules_loop(array(
    'list:select' => 'commerce-orders',
    'item:label' => t('Current list item'),
    'item:var' => 'commerce_order',
    'item:type' => 'commerce_order',
  ))->action('elavon_subscription_provide_elavon_credentials', array(
    'merchant_id' => '',
    'account' => '',
    'shared_secret' => '',
    'server_url' => '',
    'elavon_credentials:label' => t('Elavon credentials array'),
    'elavon_credentials:var' => 'elavon_credentials',
  ))->action('commerce_elavon_example_provide_data_from_order', array(
    'order:select' => 'commerce-order',
    'alias:label' => t('Alias'),
    'alias:var' => 'alias',
    'preffix:label' => t('Schedule preffix'),
    'preffix:var' => 'preffix',
    'schedule:label' => t('Schedule'),
    'schedule:var' => 'schedule',
  ))->action('elavon_subscription_provide_new_subscription', array(
    'subscription_id:select' => 'commerce-order:order-id',
    'alias:select' => 'alias',
    'payer_ref' => '[commerce-order:payer-ref]',
    'pmt_ref' => '[commerce-order:pmt-ref]',
    'preffix' => '[preffix:value]-',
    'currency:select' => 'commerce-order:commerce-order-total:currency-code',
    'amount:select' => 'commerce-order:commerce-order-total:amount',
    'schedule:select' => 'schedule',
    'startdate' => '',
    'numtimes' => '-1',
    'subscription:label' => t('A new Elavon subscription data'),
    'subscription:var' => 'subscription',
  ))->action('elavon_subscription_process_new_subscription', array(
    'subscription:select' => 'subscription',
    'credentials:select' => 'elavon-credentials',
    'response_result:label' => t('The result code of the request'),
    'response_result:var' => 'response_result',
    'response_message:label' => t('A message describing the result code'),
    'response_message:var' => 'response_message',
    'response_merchant_id:label' => t('The client id as in the payandshop..client_details table'),
    'response_merchant_id:var' => 'response_merchant_id',
  ))->action('commerce_elavon_subscription_update_order_status', array(
    'order:select' => 'commerce-order',
    'status_success' => 'commerce_realex_completed',
    'status_failure' => 'commerce_realex_error',
    'response_result:select' => 'response_result',
  ));

  // Add the loop to the rule as an action.
  $rule->action($loop);

  $configs['commerce_elavon_example_process_subscriptions'] = $rule;

  return $configs;
}