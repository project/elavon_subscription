## Summary
@todo: add sumary.

## Dependencies
 * Commerce Elavon Subscriptions
 * Commerce Realex Redirect Subscription Payments
 * List
 * Transliteration

## Configuration and usage instructions
@todo: add configuration and usage instructions.

## Sponsors
 * This Drupal 7 module is sponsored and developed by http://cambrico.net/
   Get in touch with us for customizations and consultancy:
   http://cambrico.net/contact

## About the authors
 * Pedro Cambra (https://www.drupal.org/u/pcambra)
 * Manuel Egío (https://www.drupal.org/u/facine)
