<?php

/**
 * @file
 * Rules integratrion for the Commerce Elavon Example module.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_elavon_example_rules_action_info() {
  return array(
    'commerce_elavon_example_provide_data_from_order' => array(
      'label' => t('Provide subscription data from Commerce Order'),
      'group' => t('Elavon Subscription'),
      'parameter' => array(
        'order' => array(
          'label' => t('Order', array(), array('context' => 'a drupal commerce order')),
          'type' => 'entity',
          'wrapped' => TRUE,
        ),
      ),
      'provides' => array(
        'alias' => array(
          'type' => 'text',
          'label' => t('Alias'),
        ),
        'preffix' => array(
          'type' => 'text',
          'label' => t('Schedule preffix'),
        ),
        'schedule' => array(
          'type' => 'text',
          'label' => t('Schedule'),
        ),
      ),
    ),
  );
}

/**
 * Rules action: Provide subscription data from Commerce Order.
 *
 * @param commerce_order $order
 *   Wrapped commerce_order entity type.
 *
 * @return string $alias
 *   An alias or name given to this schedule - this can be used to identify
 *   multiple schedules for ease of identification. For example, "gym
 *   membership" or "monthly subscription".
 * @return string $preffix
 *   This will be used as the prefix of the order id. The order id will be
 *   constructed as follows: orderidstub-scheduleref-runid-attemptid.
 * @return string $schedule
 *   The schedule to follow. Must consist of 3 space/tab separated items - one
 *   each for day-of-month, month and day-of-week or a macro name.
 */
function commerce_elavon_example_provide_data_from_order($order_wrapper) {
  $order_data = array(
    'alias' => '',
    'preffix' => '',
    'schedule' => '',
  );

  // Search the first line item with a "suscripcion" product type.
  foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
    if ($line_item_wrapper->commerce_product->type->value() === 'suscripcion') {
      return array(
        'alias' => _commerce_elavon_example_remove_illegal_characters_from_alias($line_item_wrapper->commerce_product->title->value()),
        'preffix' => $order_wrapper->order_id->value(),
        'schedule' => _commerce_elavon_example_prepare_period($line_item_wrapper->commerce_product->elavon_schedule_interval->value()),
      );
    }
  }

  // Return an empty array data if there isn't any suscription product in cart.
  return $order_data;
}
